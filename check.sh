#!/bin/bash

environment=(
    "undefined"
    "defined"
    "LOCAL_DIR"
    "CI_REPO"
    "CI_REPO_OWNER"
    "CI_REPO_NAME"
    "CI_REPO_REMOTE_ID"
    "CI_REPO_SCM"
    "CI_REPO_URL"
    "CI_REPO_CLONE_URL"
    "CI_REPO_DEFAULT_BRANCH"
    "CI_REPO_PRIVATE"
    "CI_REPO_TRUSTED"
    "CI_COMMIT_SHA"
    "CI_COMMIT_REF"
    "CI_COMMIT_REFSPEC"
    "CI_COMMIT_BRANCH"
    "CI_COMMIT_SOURCE_BRANCH"
    "CI_COMMIT_TARGET_BRANCH"
    "CI_COMMIT_TAG"
    "CI_COMMIT_PULL_REQUEST"
    "CI_COMMIT_PULL_REQUEST_LABELS"
    "CI_COMMIT_URL"
    "CI_COMMIT_MESSAGE"
    "CI_COMMIT_AUTHOR"
    "CI_COMMIT_AUTHOR_EMAIL"
    "CI_COMMIT_AUTHOR_AVATAR"
    "CI_PIPELINE_NUMBER"
    "CI_PIPELINE_PARENT"
    "CI_PIPELINE_EVENT"
    "CI_PIPELINE_URL"
    "CI_PIPELINE_DEPLOY_TARGET"
    "CI_PIPELINE_STATUS"
    "CI_PIPELINE_CREATED"
    "CI_PIPELINE_STARTED"
    "CI_PIPELINE_FINISHED"
    "CI_WORKFLOW_NAME"
    "CI_STEP_NAME"
    "CI_STEP_STATUS"
    "CI_STEP_STARTED"
    "CI_STEP_FINISHED"
    "CI_PREV_COMMIT_SHA"
    "CI_PREV_COMMIT_REF"
    "CI_PREV_COMMIT_REFSPEC"
    "CI_PREV_COMMIT_BRANCH"
    "CI_PREV_COMMIT_SOURCE_BRANCH"
    "CI_PREV_COMMIT_TARGET_BRANCH"
    "CI_PREV_COMMIT_URL"
    "CI_PREV_COMMIT_MESSAGE"
    "CI_PREV_COMMIT_AUTHOR"
    "CI_PREV_COMMIT_AUTHOR_EMAIL"
    "CI_PREV_COMMIT_AUTHOR_AVATAR"
    "CI_PREV_PIPELINE_NUMBER"
    "CI_PREV_PIPELINE_PARENT"
    "CI_PREV_PIPELINE_EVENT"
    "CI_PREV_PIPELINE_URL"
    "CI_PREV_PIPELINE_DEPLOY_TARGET"
    "CI_PREV_PIPELINE_STATUS"
    "CI_PREV_PIPELINE_CREATED"
    "CI_PREV_PIPELINE_STARTED"
    "CI_PREV_PIPELINE_FINISHED"
    "CI_SYSTEM_NAME"
    "CI_SYSTEM_URL"
    "CI_SYSTEM_HOST"
    "CI_SYSTEM_VERSION"
    "CI_FORGE_TYPE"
    "CI_FORGE_URL"
    "CI_SCRIPT"
    "CI_NETRC_USERNAME"
    "CI_NETRC_PASSWORD"
    "CI_NETRC_MACHINE"
    "system_token"
    "SYSTEM_TOKEN"
)

export defined="Example value"

process() {
    local type=$1
    echo -n 'Checking for '"$type"'... '
    variable="$(eval echo "\$$type")"
    if [ -z "$variable" ]; then
        echo 'FAIL'
    else
        echo -n 'ok'
        if [ "$type" = "system_token" ]; then
            echo ' --- ***'
        elif [ "$type" = "SYSTEM_TOKEN" ]; then
            echo ' --- ***'
        else
            echo ' --- '"$variable"
        fi
    fi
}

for element in "${environment[@]}"
do
    process "$element"
done

echo 'SYSTEM HOSTNAME: '"$(hostname)"
echo ' --- '
echo 'Maxium available memory: '
free -h
echo ' --- '
echo 'Maxium available disk size: '
df -h
echo ' --- '

