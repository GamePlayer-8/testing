FROM alpine AS builder

RUN echo a

FROM alpine

COPY --from=builder /bin/sh .

CMD ["/bin/sh"]
