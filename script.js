function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

function createRandomDot() {
    const dot = document.createElement('div');
    dot.className = 'dot';
    const size = getRandomInt(5, 20); // Random dot size between 5 and 20 pixels
    const x = getRandomInt(0, window.innerWidth);
    const y = getRandomInt(0, window.innerHeight);

    dot.style.width = `${size}px`;
    dot.style.height = `${size}px`;
    dot.style.left = `${x}px`;
    dot.style.top = `${y}px`;

    return dot;
}

function createLine(dot1, dot2) {
    const line = document.createElement('div');
    line.className = 'line';

    const x1 = parseInt(dot1.style.left);
    const y1 = parseInt(dot1.style.top);
    const x2 = parseInt(dot2.style.left);
    const y2 = parseInt(dot2.style.top);

    const length = Math.sqrt((x2 - x1) ** 2 + (y2 - y1) ** 2);
    line.style.width = `${length}px`;

    const angle = Math.atan2(y2 - y1, x2 - x1);
    line.style.transform = `rotate(${angle}rad)`;
    line.style.left = `${x1}px`;
    line.style.top = `${y1}px`;

    return line;
}

document.addEventListener('DOMContentLoaded', () => {
    const background = document.querySelector('.background');

    for (let i = 0; i < 100; i++) { // Adjust the number of dots as needed
        const dot = createRandomDot();
        background.appendChild(dot);
    }

    const dots = document.querySelectorAll('.dot');

    for (let i = 0; i < dots.length - 1; i++) {
        for (let j = i + 1; j < dots.length; j++) {
            const line = createLine(dots[i], dots[j]);
            background.appendChild(line);
        }
    }
});
